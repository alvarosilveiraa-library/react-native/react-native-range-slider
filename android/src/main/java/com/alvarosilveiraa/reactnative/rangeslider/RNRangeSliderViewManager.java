package com.alvarosilveiraa.reactnative.rangeslider;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.common.MapBuilder;
import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.annotations.ReactProp;
import com.facebook.react.uimanager.events.RCTEventEmitter;

import java.util.Map;

import javax.annotation.Nullable;

public class RNRangeSliderViewManager extends SimpleViewManager<RNRangeSlider> {
    private static final String ON_VALUE_CHANGED_EVENT_NAME = "onValueChanged";
    private static final String REACT_CLASS = "RNRangeSlider";

    @Override
    public String getName() {
        return REACT_CLASS;
    }

    @ReactProp(name = "rangeEnabled")
    public void setRangeEnabled(RNRangeSlider view, boolean enabled) {
        view.setRangeEnabled(enabled);
    }

    @ReactProp(name = "gravity")
    public void setGravity(RNRangeSlider view, String gravity) {
        view.setGravity(gravity);
    }

    @ReactProp(name = "min")
    public void setMin(RNRangeSlider view, int min) {
        view.setMinValue(min);
    }

    @ReactProp(name = "max")
    public void setMax(RNRangeSlider view, int max) {
        view.setMaxValue(max);
    }

    @ReactProp(name = "step")
    public void setStep(RNRangeSlider view, int step) {
        view.setStep(step);
    }

    @ReactProp(name = "highValue")
    public void setHighValue(RNRangeSlider view, int value) {
        view.setHighValue(value);
    }

    @ReactProp(name = "lowValue")
    public void setLowValue(RNRangeSlider view, int value) {
        view.setLowValue(value);
    }

    @ReactProp(name = "lineWidth")
    public void setLineWidth(RNRangeSlider view, float width) {
        view.setLineWidth(width);
    }

    @ReactProp(name = "thumbRadius")
    public void setThumbRadius(RNRangeSlider view, float radius) {
        view.setThumbRadius(radius);
    }

    @ReactProp(name = "thumbBorderWidth")
    public void setThumbBorderWidth(RNRangeSlider view, float width) {
        view.setThumbBorderWidth(width);
    }

    @ReactProp(name = "labelStyle")
    public void setLabelStyle(RNRangeSlider view, String style) {
        view.setLabelStyle(style);
    }

    @ReactProp(name = "labelGapHeight")
    public void setLabelGapHeight(RNRangeSlider view, float gapHeight) {
        view.setLabelGapHeight(gapHeight);
    }

    @ReactProp(name = "labelTailHeight")
    public void setLabelTailHeigh(RNRangeSlider view, float tailHeight) {
        view.setLabelTailHeight(tailHeight);
    }

    @ReactProp(name = "labelFontSize")
    public void setLabelFontSize(RNRangeSlider view, float size) {
        view.setTextSize(size);
    }

    @ReactProp(name = "labelBorderWidth")
    public void setLabelBorderWidth(RNRangeSlider view, float width) {
        view.setLabelBorderWidth(width);
    }

    @ReactProp(name = "labelPadding")
    public void setLabelPadding(RNRangeSlider view, float padding) {
        view.setLabelPadding(padding);
    }

    @ReactProp(name = "labelBorderRadius")
    public void setLabelBorderRadius(RNRangeSlider view, float radius) {
        view.setLabelBorderRadius(radius);
    }

    @ReactProp(name = "textFormat")
    public void setLabelTextFormat(RNRangeSlider view, String format) {
        view.setTextFormat(format);
    }

    @ReactProp(name = "blankColor")
    public void setBlankColor(RNRangeSlider view, String hexColor) {
        view.setBlankColor(hexColor);
    }

    @ReactProp(name = "selectionColor")
    public void setSelectionColor(RNRangeSlider view, String hexColor) {
        view.setSelectionColor(hexColor);
    }

    @ReactProp(name = "thumbColor")
    public void setThumbColor(RNRangeSlider view, String hexColor) {
        view.setThumbColor(hexColor);
    }

    @ReactProp(name = "thumbBorderColor")
    public void setThumbBorderColor(RNRangeSlider view, String hexColor) {
        view.setThumbBorderColor(hexColor);
    }

    @ReactProp(name = "labelTextColor")
    public void setLabelTextColor(RNRangeSlider view, String hexColor) {
        view.setLabelTextColor(hexColor);
    }

    @ReactProp(name = "labelBackgroundColor")
    public void setLabelBackgroundColor(RNRangeSlider view, String hexColor) {
        view.setLabelBackgroundColor(hexColor);
    }

    @ReactProp(name = "labelBorderColor")
    public void setLabelBorderColor(RNRangeSlider view, String hexColor) {
        view.setLabelBorderColor(hexColor);
    }

    @Override
    protected RNRangeSlider createViewInstance(final ThemedReactContext reactContext) {
        final RNRangeSlider slider = new RNRangeSlider(reactContext);

        slider.setOnValueChangeListener(new RNRangeSlider.OnValueChangeListener() {
            @Override
            public void onValueChanged(int lowValue, int highValue, boolean fromUser) {
                WritableMap event = Arguments.createMap();
                event.putInt("lowValue", lowValue);
                event.putInt("highValue", highValue);
                event.putBoolean("fromUser", fromUser);

                reactContext.getJSModule(RCTEventEmitter.class).receiveEvent(slider.getId(), ON_VALUE_CHANGED_EVENT_NAME, event);
            }
        });

        return slider;
    }

    @Nullable
    @Override
    public Map<String, Object> getExportedCustomDirectEventTypeConstants() {
        return MapBuilder.<String, Object>builder().put(ON_VALUE_CHANGED_EVENT_NAME, MapBuilder.of("registrationName", ON_VALUE_CHANGED_EVENT_NAME)).build();
    }
}
