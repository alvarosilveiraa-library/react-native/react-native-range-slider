require 'json'

package = JSON.parse(File.read('package.json'))

Pod::Spec.new do |s|
  s.name          = package["name"]
  s.version       = package["version"]
  s.summary       = package["description"]
  s.homepage      = "https://gitlab.com/alvarosilveiraa-library/react-native/react-native-range-slider"
  s.license       = package["license"]
  s.author        = { "author" => package["author"] }
  s.platform      = :ios, "7.0"
  s.source        = { :git => "#{package["repository"]["url"]}", :tag => "#{s.version}" }
  s.source_files  = "ios/RNRangeSlider/**/*.{h,m}"
  s.requires_arc  = true

  s.dependency "React"
end
