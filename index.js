import React from 'react';
import {
  requireNativeComponent
} from 'react-native';

const RNRangeSlider = requireNativeComponent('RNRangeSlider');

export default class RangeSlider extends React.PureComponent {
  state = {
    lowValue: 0
  };

  componentDidMount() {
    this.setLowValue(this.props.lowValue);
  }

  setLowValue(lowValue) {
    this.setState({ lowValue });
  }

  render() {
    const onValueChanged = this.props.onValueChanged;

    return (
      <RNRangeSlider
        {...this.props}
        lowValue={this.state.lowValue}
        onValueChanged={event => {
          if(typeof onValueChanged === 'function') {
            const {
              lowValue,
              highValue,
              fromUser
            } = event.nativeEvent;

            onValueChanged(lowValue, highValue, fromUser);
          }
        }}
      />
    );
  }
}
